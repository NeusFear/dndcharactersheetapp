import React, { Component } from "react";
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import MainMenu from './pages/MainMenu';
import NavigationService from './scripts/NavigationService';
import CharacterRouter from './pages/MainCharacter';
import MainCreation from './pages/MainCreation'
import StorageService from "./scripts/StorageService";

//This is the app that loads everything else into it.

//Create the navigator logic, it handles the page routes
export default class App extends Component {

  render() {

    //Setup StorageService if needed
    StorageService.trySetup();

    //Reset the temp character stuff
    StorageService.resetBuilder();

    return(
      <AppContentContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}

const MainNavigator = createSwitchNavigator({
  Landing: {screen: MainMenu},
  Character: CharacterRouter,
  Creator: MainCreation
},
{
  initialRouteName: "Landing"
});

const AppContentContainer = createAppContainer(MainNavigator);