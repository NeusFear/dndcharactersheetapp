import { AsyncStorage } from "react-native";
import Utility from "./Utility";

//Temporary Storage Things for the character storage
//Save the character name to the temp character storage
setTempName = async name => {
  try {
    await AsyncStorage.setItem("temp::name", name);
  } catch (error) {
    console.log(error);
    alert("There was an error saving the temp character name");
  }
};

//Save the class to the temp class storage
setTempClass = async clazz => {
  try {
    await AsyncStorage.setItem("temp::class", clazz);
  } catch (error) {
    console.log(error);
    alert("There was an error setting the temp class");
  }
};

//Save the race to the temp class storage
setTempRace = async race => {
  try {
    await AsyncStorage.setItem("temp::race", race);
  } catch (error) {
    console.log(error);
    alert("There was an error setting the temp race");
  }
};

//Save the background to the temp background storage
setTempBackground = async background => {
  try {
    await AsyncStorage.setItem("temp::background", background);
  } catch (error) {
    console.log(error);
    alert("There was an error setting the temp background");
  }
};

resetBuilder = async () => {
  try {
    await AsyncStorage.removeItem("temp::name");
    await AsyncStorage.removeItem("temp::race");
    await AsyncStorage.removeItem("temp::class");
    await AsyncStorage.removeItem("temp::background");
    await AsyncStorage.removeItem("current::id");
  } catch {
    console.log(error);
  }
};

//Save the field to character storage
saveCharacterFromCreator = async () => {
  try {
    console.log("1. Starting Save");

    var name = await AsyncStorage.getItem("temp::name");
    var clazz = await AsyncStorage.getItem("temp::class");
    var race = await AsyncStorage.getItem("temp::race");
    var background = await AsyncStorage.getItem("temp::background");

    console.log(
      "2. Set Variables: " + name + " " + clazz + " " + race + " " + background
    );

    var tempCharacter = {};
    var tempCharacterString = "";

    console.log("3. Starting Save to tempCharacter");

    if (name != null && clazz != null && race != null && background != null) {
      tempCharacter.name = name;
      tempCharacter.class = clazz;
      tempCharacter.race = race;
      tempCharacter.background = background;

      console.log("4. Assigning an ID to " + tempCharacter.name);
      tempCharacter.key = Utility.createGUID();
      console.log("5. ID Addigned: " + tempCharacter.key);

      tempCharacterString = JSON.stringify({
        name: name,
        class: clazz,
        level: 0,
        key: tempCharacter.key
      });
    }

    console.log(
      "6. Set Variables to tempCharacter: " +
        tempCharacter.name +
        " " +
        tempCharacter.class +
        " " +
        tempCharacter.race +
        " " +
        tempCharacter.background +
        " " +
        tempCharacter.key
    );

    if (
      tempCharacter.name != null &&
      tempCharacter.class != null &&
      tempCharacter.race != null &&
      tempCharacter.background != null
    ) {
      console.log("7. Getting Characters list from AsyncStorage.");
      var characters = await getCharacters();
      var charactersString = JSON.stringify(characters);
      console.log("8. Characters: " + characters.length);
      console.log("9. Characters: " + charactersString);

      console.log(
        "10. Pushing new Character to Characters. " +
          charactersString.substring(0, charactersString.length - 1) +
          ", " +
          tempCharacterString +
          "]"
      );
      await AsyncStorage.setItem(
        "characters",
        charactersString.substring(0, charactersString.length - 1) +
          ", " +
          tempCharacterString +
          "]"
      );

      //Allows us to open the character after the builder right away.
      await saveCurrentID(tempCharacter.key);

      //save the actual character object here
    } else {
      console.log("Not all fields of the builder are being used.");
    }
  } catch (error) {
    console.log(error);
    alert("There was an error saving the character to storage");
  }
};

//Try setting up the storage service if it's needed
trySetup = async () => {
  try {
    console.log("Getting characters.");
    const value = await getCharacters();
    if (value === null) {
      console.log("StorageService needs Setup.");
      setup();
      console.log("Finished Setting up StorageService.");
    }
    console.log("StorageService is Setup.");
    deleteAllCharacters();
    console.log("Deleting all characters on app startup for debug purposes.");
  } catch (error) {
    console.log(error);
  }
};

setup = async () => {
  try {
    await AsyncStorage.setItem("characters", JSON.stringify([]));
  } catch (error) {
    console.log(error);
  }
};

//Load character data from character storage from ID
getCharacters = async () => {
  //return([ {key: {'name': "Joe", 'class': "John", 'level': "10", 'id': "srghrgh"}}, {key: {'name': "Joe", 'class': "John", 'level': "10", 'id': "srghrgh"}} ]);
  try {
    const value = await AsyncStorage.getItem("characters");
    console.log("StorageService: getCharacters : " + value);
    return JSON.parse(value);
  } catch (error) {
    console.log(error);
    alert("There was an error loading characters.");
  }
};

//Save character from temp storage to characters list
function addCharacter() {
  console.log("add");
}

deleteAllCharacters = async () => {
  try {
    await AsyncStorage.setItem(
      "characters",
      JSON.stringify([
        {
          name: "Grammen",
          class: "Fighter",
          level: "10",
          key: Utility.createGUID()
        }
      ])
    );
  } catch (error) {
    console.log(error);
  }
};

async function saveCurrentID(id) {
  try {
    await AsyncStorage.setItem("current::id", id);
  }
  catch (error) {
    console.log(error);
  }
}

getCurrentID = async () => {
  try {
    const id = await AsyncStorage.getItem("current::id");
    console.log("getCurrentID Result: " + id);
    if (id !== null) {
      return id;
    } else {
      return "none";
    }
  }
  catch (error) {
    console.log(error);
  }
}

export default {
  trySetup,
  getCharacters,
  setTempName,
  setTempClass,
  setTempRace,
  setTempBackground,
  resetBuilder,
  saveCharacterFromCreator,
  deleteAllCharacters,
  getCurrentID,
  saveCurrentID
};
