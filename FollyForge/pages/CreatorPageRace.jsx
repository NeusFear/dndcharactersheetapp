import React, { Component } from "react";
import { Text } from "react-native";
import { Card, Button, ButtonText } from "../AppStylesheet";
import StorageService from "../scripts/StorageService";

class CreatorPageRace extends Component {
    
  submitRace = async (race) => {
    await StorageService.setTempRace(race || "half-orc");
    this.props.navigation.navigate("CreatorBackground");
  };

  render() {
    return (
      <Card>
        <Text>Choose a Race</Text>
        <Button
          onPress={() => {
            this.submitRace();
          }}
        >
          <ButtonText>Next</ButtonText>
        </Button>
      </Card>
    );
  }
}

export default CreatorPageRace;
