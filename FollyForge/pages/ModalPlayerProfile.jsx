import React, { Component } from "react";
import { Modal, View, TouchableHighlight, Text } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import {
  ModalContainer,
  ModalContent,
  Container,
  Card,
  TitleBar,
  TitleBarText,
  Columns,
  Column
} from "../AppStylesheet";
import colors from "../AppColors";

//This modal is used to display the character features.
//  Character trais, Alignment, Flaws, etc.
export default class ProfileMenu extends Component {
  //Start the modal as hidden
  state = {
    modalVisible: false
  };

  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
        >
          <ModalContainer>
            <TitleBar style={{ height: 20 }}>
              <Columns>
                <Column></Column>
                <Column center>
                  <TitleBarText>Character</TitleBarText>
                </Column>
                <Column right>
                  <TouchableHighlight
                    onPress={() => {
                      this.setState({ modalVisible: !this.state.modalVisible });
                    }}
                  >
                    <Icon name="x" color={colors.GRAY} size={26} />
                  </TouchableHighlight>
                </Column>
              </Columns>
            </TitleBar>

            <ModalContent>
              <Text>Hello There!</Text>
            </ModalContent>
          </ModalContainer>
        </Modal>

        <TouchableHighlight
          onPress={() => {
            this.setState({ modalVisible: true });
          }}
        >
          <Icon name="user" color={colors.GRAY} size={26} />
        </TouchableHighlight>
      </View>
    );
  }
}
