import React, { Component } from "react";
import { Modal, View, TouchableHighlight, Text } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import NavigationService from "../scripts/NavigationService";
import {
  ModalContainer,
  ModalContent,
  Container,
  Card,
  TitleBar,
  TitleBarText,
  Columns,
  Column,
  Button,
  ButtonText
} from "../AppStylesheet";
import colors from "../AppColors";

//This modal is used to house all the navigation and option buttons for the app that don't have to do with characters.
export default class Menu extends Component {
  //Start the modal as hidden
  state = {
    modalVisible: false
  };

  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
        >
          <ModalContainer>
            <TitleBar style={{ height: 20 }}>
              <Columns>
                <Column>
                  <TouchableHighlight
                    onPress={() => {
                      this.setState({ modalVisible: !this.state.modalVisible });
                    }}
                  >
                    <Icon name="x" color={colors.GRAY} size={26} />
                  </TouchableHighlight>
                </Column>
                <Column center>
                  <TitleBarText>Menu</TitleBarText>
                </Column>
                <Column></Column>
              </Columns>
            </TitleBar>

            <ModalContent>
              <Container>
                <Button
                  menu
                  onPress={() => {
                    NavigationService.navigate("Landing");
                  }}
                >
                  <ButtonText>Characters</ButtonText>
                </Button>
                <Button menu type={colors.BLUE2}>
                  <ButtonText>Help</ButtonText>
                </Button>
              </Container>
            </ModalContent>
          </ModalContainer>
        </Modal>

        <TouchableHighlight
          onPress={() => {
            this.setState({ modalVisible: true });
          }}
        >
          <Icon name="menu" color={colors.GRAY} size={26} />
        </TouchableHighlight>
      </View>
    );
  }
}
