import React, { Component } from "react";
import { Text } from "react-native";
import { Card, Button, ButtonText } from "../AppStylesheet";
import StorageService from "../scripts/StorageService";

class CreatorPageClass extends Component {
    
  submitClass = async (clazz) => {
    await StorageService.setTempClass(clazz || "Paladain");
    this.props.navigation.navigate("CreatorRace");
  };

  render() {
    return (
      <Card>
        <Text>Pick your Class</Text>
        <Button
          onPress={() => {
            this.submitClass();
          }}
        >
          <ButtonText>Next</ButtonText>
        </Button>
      </Card>
    );
  }
}

export default CreatorPageClass;
