import React, { Component } from "react";
import { Text, TextInput } from "react-native";
import { Card, Button, ButtonText, InputField, Title } from "../AppStylesheet";
import StorageService from "../scripts/StorageService";

class CreatorPageName extends Component {

  state = {
    name: ""
  };

  submitName = async (name) => {
    await StorageService.setTempName(name == "" ? "Unnamed" : name);
    this.props.navigation.navigate("CreatorClass");
  };

  updateName(text) {
    this.setState({name: text})
  }

  render() {
    return (
      <Card>
        <Title>Name Your Character</Title>
        <InputField style={{ height: 40, borderColor: 'gray', borderWidth: 1, width: 100 }} onChangeText={ text => this.updateName(text) } />
        <Button wide
          onPress={() => {
            this.submitName(this.state.name);
          }}
        >
          <ButtonText>Next</ButtonText>
        </Button>
      </Card>
    );
  }
}

export default CreatorPageName;
