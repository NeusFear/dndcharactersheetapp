import React, { Component } from "react";
import { Text } from "react-native";
import { Container, Card } from "../AppStylesheet"
import StorageService from "../scripts/StorageService";;

//This page lists all fo a character's equipemnt
class EquipmentPage extends Component {

  state = {
    id: "no-id-set"
  };

  async componentDidMount() {
    const id = await StorageService.getCurrentID();
    this.setState({ id: id });
  }

  render() {
    if (this.state.id === "no-id-set") {
      return null;
    } else {
      return (
        <Card>
          <Text>Equipment</Text>
          <Text>{this.state.id}</Text>
        </Card>
      );
    }
  }
}

export default EquipmentPage;
