import React, { Component } from "react";
import { Text } from "react-native";
import { Card, Button, ButtonText } from "../AppStylesheet";
import StorageService from "../scripts/StorageService";

class CreatorPageBackground extends Component {
    
  submitBackground = async (background) => {
    await StorageService.setTempBackground(background || "Outlander");
    this.props.navigation.navigate("CreatorComplete");
  };

  render() {
    return (
      <Card>
        <Text>Choose a Background</Text>
        <Button
          onPress={() => {
            this.submitBackground();
          }}
        >
          <ButtonText>Next</ButtonText>
        </Button>
      </Card>
    );
  }
}

export default CreatorPageBackground;
