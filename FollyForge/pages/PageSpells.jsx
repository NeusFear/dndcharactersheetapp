import React, { Component } from "react";
import { Text } from "react-native";
import { Container, Card } from "../AppStylesheet";
import StorageService from "../scripts/StorageService";

//This page houses all the spells that you currently have preapared
//TODO  This will need a modal for adding from the srd spells and custom added spells
class SpellsPage extends Component {

  state = {
    id: "no-id-set"
  };

  async componentDidMount() {
    const id = await StorageService.getCurrentID();
    this.setState({ id: id });
  }

  render() {
    if (this.state.id === "no-id-set") {
      return null;
    } else {
      return (
        <Card>
          <Text>Spells</Text>
          <Text>{this.state.id}</Text>
        </Card>
      );
    }
  }
}

export default SpellsPage;
