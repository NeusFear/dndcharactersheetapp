import React, { Component } from "react";
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Dimensions
} from "react-native";
import * as Font from "expo-font";

const { height, width } = Dimensions.get("window");

//This set of pages is going to be converted into a page similar to the character sheet in that it's a bunch of swipable pages
//These pages will have different aspects of the character creator in it, like a tutorial
//You can swipe between them and they will be full of nice and friendly graphics
//It's a modal so it can be referenced any time, which is nice.

//NOTE: This page is way out of date.

class LandingPage extends Component {
  state = {
    fontLoaded: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      BigJohn: require("../assets/fonts/BigJohnPRO-Bold.otf"),
      SlimJoe: require("../assets/fonts/BigJohnPRO-Light.otf"),
      RegularJim: require("../assets/fonts/BigJohnPRO-Regular.otf")
    });

    this.setState({ fontLoaded: true });
  }

  render() {
    return (
      <View style={styles.fullScreen}>
        <ImageBackground
          source={require("../assets/images/splashscreen.png")}
          style={{
            width: "100%",
            height: "100%",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          {this.state.fontLoaded ? (
            <View style={{ width: "80%", alignItems: "center" }}>
              <Text style={styles.heading}>Hello, world!</Text>
              <TouchableOpacity
                onPress={this.handleButtonPress}
                style={{
                  width: "90%",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Text style={styles.button}>GET STARTED</Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </ImageBackground>
      </View>
    );
  }

  handleButtonPress() {
    alert("test");
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: "white",
    width: "90%",
    textAlign: "center",
    padding: 14,
    fontSize: 16,
    fontWeight: "bold",
    color: "#545454",
    borderRadius: 24,
    borderWidth: 1,
    borderColor: "white",
    overflow: "hidden",
    fontFamily: "BigJohn"
  },
  heading: {
    color: "white",
    fontSize: 48,
    fontFamily: "BigJohn"
  },
  fullScreen: {
    width: width,
    height: height
  }
});

export default LandingPage;
