import React, { Component } from "react";
import Icon from "react-native-vector-icons/Feather";
import { createMaterialTopTabNavigator } from "react-navigation-tabs";
import {
  AppContainer,
  AppStatusBar,
  Main,
  TitleBar,
  TitleBarText,
  Container,
  Columns,
  Column
} from "../AppStylesheet";
import colors from "../AppColors";
import EquipmentPage from "./PageEquipment";
import GeneralPage from "./PageGeneral";
import SpellsPage from "./PageSpells";
import Menu from "./ModalMenu";
import ProfileMenu from "./ModalPlayerProfile";

//Router Logic
//  Displays the 3 pages of the character sheet
//  These are loaded into the Character route in App.js
const AppTabNavigator = createMaterialTopTabNavigator(
  {
    General: {
      screen: GeneralPage,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="activity" color={tintColor} size={24} />
        )
      }
    },
    Equipment: {
      screen: EquipmentPage,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="pocket" color={tintColor} size={24} />
        )
      }
    },
    Spells: {
      screen: SpellsPage,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="book-open" color={tintColor} size={24} />
        )
      }
    }
  },
  {
    initialRouteName: "General",
    tabBarPosition: "bottom",
    swipeEnabled: true,
    tabBarOptions: {
      activeTintColor: colors.LIGHT,
      inactiveTintColor: colors.GRAY,
      style: {
        backgroundColor: colors.DARKDARK
      },
      indicatorStyle: {
        height: 4,
        backgroundColor: colors.BLUE,
        borderRadius: 2
      },
      showIcon: true,
      showLabel: false
    }
  }
);

//This is the app that hold all the character sheet interfaces.
//  The AppContainer is what holds all the main components
//  The AppStatus Bar allows us to make sure the batterey level and stuff is always visible
//  The TitleBar holds the bar at the top of the Character Sheet with menu and profile buttons
//  The AppTabNavigator routes to all the pages on the charactersheet
export default class CharacterRouter extends React.Component {
  static router = AppTabNavigator.router;

  render() {
    return (
      <AppContainer>
        <AppStatusBar />
        <TitleBar>
          <Columns>
            <Column>
              <Menu />
            </Column>
            <Column center size="10">
              <TitleBarText>
                {this.props.navigation.state.params.name}
              </TitleBarText>
            </Column>
            <Column right>
              <ProfileMenu />
            </Column>
          </Columns>
        </TitleBar>
        <Main>
          <AppTabNavigator navigation={this.props.navigation} />
        </Main>
      </AppContainer>
    );
  }
}
