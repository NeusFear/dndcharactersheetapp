import React, { Component } from "react";
import { Text, View } from "react-native";
import { Container, Card, Button, ButtonText } from "../AppStylesheet";
import NavigationService from "../scripts/NavigationService";
import Colors from "../AppColors";
import StorageService from "../scripts/StorageService";

//This page is used to display all the general information about a character
//  Health, Ability Scores, AC etc. The stuff you look at most
class GeneralPage extends React.Component {

  state = {
    id: "no-id-set"
  };

  async componentDidMount() {
    const id = await StorageService.getCurrentID();
    this.setState({id: id});
  }

  render() {
    if (this.state.id === "no-id-set") {
      return null;
    } else {
      return (
        <Container>
          <Card>
            <Text>General</Text>
            <Button
              onPress={() => {
                NavigationService.navigate("Landing");
              }}
              type={Colors.PURPLE2}
            >
              <ButtonText>Hello, World!</ButtonText>
            </Button>
            <Text>{this.state.id}</Text>
          </Card>
        </Container>
      );
    }
  }
}

export default GeneralPage;
