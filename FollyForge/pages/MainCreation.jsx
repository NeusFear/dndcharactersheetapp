import React, { Component } from "react";
import {
  Modal,
  View,
  TouchableHighlight,
  Text,
  Alert,
  ScrollView,
  Dimensions
} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import {
  ModalContainer,
  ModalContent,
  Container,
  Card,
  TitleBar,
  TitleBarText,
  Columns,
  Column,
  ListItemClickable,
  Title,
  PageView,
  Button,
  ButtonText,
  AppContainer,
  AppStatusBar,
  Main
} from "../AppStylesheet";
import colors from "../AppColors";
import StorageService from "../scripts/StorageService";
import { createStackNavigator } from "react-navigation-stack";
import CreatorPageName from "./CreatorPageName";
import CreatorPageClass from "./CreatorPageClass";
import CreatorPageRace from "./CreatorPageRace";
import CreatorPageBackground from "./CreatorPageBackground";
import CreatorPageComplete from "./CreatorPageComplete";

class CancelButton extends React.Component {
  render() {
    return(
      <TouchableHighlight
        onPress={() => {
          confirmCancel();
        }}
      >
        <Icon name="x" color={colors.GRAY} size={26} />
      </TouchableHighlight>
    );
  }
}

class BackButton extends React.Component {
  render() {
    return(
      <Icon name="chevron-left" color={colors.GRAY} size={26} />
    );
  }
}

const CreatorStackNavigator = createStackNavigator(
  {
    CreatorName: {
      screen: CreatorPageName
    },
    CreatorClass: {
      screen: CreatorPageClass
    },
    CreatorRace: {
      screen: CreatorPageRace
    },
    CreatorBackground: {
      screen: CreatorPageBackground
    },
    CreatorComplete: {
      screen: CreatorPageComplete
    }
  },
  {
    initialRouteName: "CreatorName",
    defaultNavigationOptions: {
      headerStatusBarHeight: 20,
      headerLeftContainerStyle: {
        flex: 1,
        padding: 20,
        marginTop: -20
      },
      headerRightContainerStyle: {
        flex: 1,
        padding: 20,
        marginTop: -20
      },
      headerTitleAlign: "center",
      headerStyle: {
        backgroundColor: colors.DARK,
      },
      headerTintColor: colors.LIGHT,
      headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1
      },
      headerBackImage: () => <BackButton />,
      headerBackTitleVisible: false,
      title: "CharacterCreator",
      headerRight: () => <CancelButton />,
    }
  }
);

function confirmCancel() {
  Alert.alert(
    "Confirm",
    "You are about to exit the Character Creator",
    [
      { text: "Cancel" },
      {
        text: "Exit",
        onPress: () => {
          exitCreator();
        }
      }
    ],
    { cancelable: false }
  );
}

let navigator;

function exitCreator(done) {
  if (done || false) {
    navigator.navigate("Character", {
      id: "key",
      name: "name"
    });
  } else {
    navigator.navigate("Landing");
  }
}

//This modal is used to display the character features.
//  Character trais, Alignment, Flaws, etc.
export default class MainCreation extends Component {
  static router = CreatorStackNavigator.router;

  componentDidMount() {
    navigator = this.props.navigation;
    StorageService.resetBuilder();
  }

  render() {
    return (
      <AppContainer>
        <AppStatusBar />
        <Main>
          <CreatorStackNavigator navigation={this.props.navigation} screenProps={{close: () => {this.confirmCancel();}}} />
        </Main>
      </AppContainer>
    );
  }
}
