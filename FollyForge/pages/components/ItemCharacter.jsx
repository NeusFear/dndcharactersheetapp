import React, { Component } from "react";
import { Image, Text } from "react-native";
import {
  ListItem,
  Columns,
  Column,
  Container,
  Title,
  ListItemClickable
} from "../../AppStylesheet";

//This is the Character List item that displays on the MainMenu.jsx page
export class CharacterItem extends Component {
  render() {
    return (
      <ListItemClickable onPress={this.props.onPress} onLongPress={this.props.onLongPress}>
        <Columns>
          <Column>
            <Image
              style={{ width: 80, height: 80 }}
              source={require("../../assets/app_icons/icon.png")}
            />
          </Column>
          <Column size={3}>
            <Container>
              <Title>{this.props.name}</Title>
              <Text>
                Level {this.props.level} {this.props.class}
              </Text>
            </Container>
          </Column>
        </Columns>
      </ListItemClickable>
    );
  }
}
