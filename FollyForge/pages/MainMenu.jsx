import React, { Component } from "react";
import {
  AppContainer,
  AppStatusBar,
  Main,
  TitleBar,
  TitleBarText,
  Container,
  Columns,
  Column,
  Card,
  Button,
  ButtonText,
  ListItemClickable,
  Title
} from "../AppStylesheet";
import { CharacterItem } from "./components/ItemCharacter";
import MainCreation from "./MainCreation";
import { FlatList, View, Text } from "react-native";
import StorageService from "../scripts/StorageService";
import Icon from "react-native-vector-icons/Feather";
import colors from "../AppColors";

//The Character Selection menu
class MainMenu extends Component {
  state = {
    characters: []
  };

  async componentDidMount() {
    this.updateCharacters();
  }

  updateCharacters = async () => {
    this.setState({ characters: await StorageService.getCharacters() });
    console.log("MainMenu: updated characters.");
  };

  async openCharacterSheet(key, name) {
    console.log("Saving Current ID: " + key);
    await StorageService.saveCurrentID(key);
    const id = await StorageService.getCurrentID();
    console.log("Fetched ID: " + id);

    this.props.navigation.navigate("Character", {
      id: key,
      name: name
    });
  }

  render() {
    return (
      <AppContainer>
        <AppStatusBar />
        <TitleBar>
          <Columns>
            <Column center>
              <TitleBarText>Characters</TitleBarText>
            </Column>
          </Columns>
        </TitleBar>
        <Main>
          <Container>
            <FlatList
              data={this.state.characters}
              extraData={this.state}
              renderItem={({ item }) => (
                <CharacterItem
                  class={item.class}
                  level={item.level}
                  name={item.name}
                  onPress={() => {
                    this.openCharacterSheet(item.key, item.name);
                  }}
                  onLongPress={() => {
                    alert(item.key);
                  }}
                />
              )}
              ListFooterComponent={
                <View>
                  <ListItemClickable
                    navigation={this.props.navigation}
                    updateCharacters={() => {
                      this.updateCharacters();
                    }}
                    onPress={() => {
                      this.props.navigation.navigate("Creator");
                    }}
                  >
                    <Card style={{ flexDirection: "row" }}>
                      <Icon name="plus" color={colors.GRAY} size={26} />
                      <Title>Create New Character</Title>
                    </Card>
                  </ListItemClickable>

                  <Button
                    onPress={() => {
                      StorageService.deleteAllCharacters();
                      this.updateCharacters();
                    }}
                  >
                    <ButtonText>Delete all Characters</ButtonText>
                  </Button>
                </View>
              }
              ListEmptyComponent={
                <View>
                  <Text>You have no Characters</Text>
                </View>
              }
            />
          </Container>
        </Main>
      </AppContainer>
    );
  }
}

export default MainMenu;

/*
<ScrollView>
                            <CharacterItem class="Fighter" level="10" name="Grammen" onPress={() => {this.props.navigation.navigate('Character')}} />
                            <ModalCreation />
                        </ScrollView>
                        */
