import React, { Component } from "react";
import { Text } from "react-native";
import { Card, Button, ButtonText } from "../AppStylesheet";
import StorageService from "../scripts/StorageService";

class CreatorPageComplete extends Component {
    
  submitCharacter = async () => {
    await StorageService.saveCharacterFromCreator();
    this.props.navigation.navigate("Character", {
        id: "id", name: "name"
    });
  };

  render() {
    return (
      <Card>
        <Text>You done yeet.</Text>
        <Button
          onPress={() => {
            this.submitCharacter();
          }}
        >
          <ButtonText>Next</ButtonText>
        </Button>
      </Card>
    );
  }
}

export default CreatorPageComplete;
