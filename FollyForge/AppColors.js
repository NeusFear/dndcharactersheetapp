export default {

    //Cool Grays
    WHITE: '#ffffff',
    LIGHT: '#ecf0f1',
    LIGHTGRAY: '#bec3c7',
    GRAY: '#95a5a5',
    DARKGRAY: '#7e8c8d',
    DARK: '#34495e',
    DARKDARK: '#2d3e50',

    //Colors
    RED: '#e84c3d',
    RED2: '#c1392b',
    ORANGE: '#eb974e',
    ORANGE2: '#d25400',
    YELLOW: '#f1c40f',
    YELLOW2: '#f39c11',
    GREEN: '#2dcc70',
    GREEN2: '#27ae61',
    BLUEGREEN: '#1bbc9b',
    BLUEGREEN2: '#16a086',
    BLUE: '#3598db',
    BLUE2: '#297fb8',
    PURPLE: '#ae7ac4',
    PURPLE2: '#8d44ad'
}