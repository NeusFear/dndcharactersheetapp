import React, { Component } from "react";
import { Dimensions } from "react-native";
import styled from 'styled-components';
import colors from './AppColors';

const deviceWidth = Dimensions.get('screen').width + "px";
const deviceHeight = Dimensions.get('screen').height + "px";

//The custom Componenets needed for the app viewport
export const AppStatusBar = styled.View`
    background-color: ${colors.DARKDARK};
    height: 20px;
`;
export const AppContainer = styled.View`
    flex: 1;
`;
export const TitleBar = styled.View`
    flex: 1;
    background-color: ${colors.DARK};
    flex-direction: row;
    align-items: center;
`;
export const TitleBarText = styled.Text`
    font-size: 20;
    font-weight: bold;
    color: ${colors.WHITE};
`;
export const Main = styled.View`
    flex: 12;
`;



//Common Components
export const Container = styled.View`
    flex: 1;
    padding: ${props => props.empty ? "0px" : "20px"};
`;
export const Card = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
    padding: ${props => props.empty ? "0px" : "10px"};
    margin: ${props => props.empty ? "0px" : "10px"};
    background-color: ${props => props.color || colors.LIGHT};
    border-radius: 20px;
`;
export const Columns = styled.View`
    flex: 1;
    flex-direction: row;
`;
export const Column = styled.View`
    padding: ${props => props.empty ? "0px" : "10px"};
    align-items: ${props => props.right ? "flex-end" : props.center ? "center" : "flex-start"};
    flex: ${props => props.size || 1};
`;
export const Title = styled.Text`
    color: ${props => props.color || colors.DARKDARK};
    font-weight: bold;
    font-size: 20px;
`;
export const SubTitle = styled.Text`
    color: ${props => props.color || colors.DARK};
    font-weight: bold;
    font-size: 18px;
`;
export const Normal = styled.Text`
    color: ${props => props.color || colors.DARKGRAY};
`;
export const Break = styled.View`
    width: 100%;
    height: 10;
`;
export const InputField = styled.TextInput`
    padding: 10px;
    border-radius: 20;
    min-width: 80%;
    align-items: center;
    align-content: center;
    margin: 3px;
`;



//Modal Components
export const ModalContainer = styled.View`
    background-color: ${colors.LIGHT};
    width: 100%;
    height: 100%;
    margin-top: 20;
`;
export const ModalContent = styled.View`
    flex: ${props => props.hasBottom ? 10 : 12};
`;
export const MenuItem = styled.TouchableOpacity`
    width: 100%;
    padding: 20px;
    align-items: center;
    align-content: center;
    margin-bottom: 10;
`;



//Buttons
//Button Component
//Most logic here is so that the text will set it's own color based on the button background prop
export class Button extends Component {
  renderChildren = () => {
    const { children } = this.props;
    return React.Children.map(children, (child) => React.cloneElement(child, {
      type: this.props.type,
    }));
  }
  render() {
    if(this.props.menu) {
        return <MenuItem onPress={this.props.onPress} style={{ backgroundColor: this.props.type || colors.YELLOW }}>{this.renderChildren()}</MenuItem>
    }
    return <ButtonWrapper onPress={this.props.onPress} wide={this.props.wide || false} style={{ backgroundColor: this.props.type || colors.YELLOW }}>{this.renderChildren()}</ButtonWrapper>
  }
}
//The Button Style
const ButtonWrapper = styled.TouchableOpacity`
    padding: 10px;
    border-radius: 20;
    min-width: ${props => props.wide ? "80%" : "140px"};
    align-items: center;
    align-content: center;
`;
//The Button Text Style
export const ButtonText = styled.Text`
  color: ${props => isColorLight(props.type) ? colors.DARK : colors.WHITE};
  font-size: 16;
  font-weight: bold;
`;



//List Items
//Used in Spells list and Character Selection mosly
export const ListItem = styled.View`
    height: 100px;
    background-color: ${colors.LIGHT};
    border-radius: 20px;
    width: 100%;
    margin-bottom: 10;
`;
export const ListItemClickable = styled.TouchableOpacity`
    height: 100px;
    background-color: ${colors.LIGHT};
    border-radius: 20px;
    width: 100%;
    margin-bottom: 10;
`;



//PageView Components
//Pages on a single scroll view
export const PageView = styled.View`
    width: ${deviceWidth};
    flex-direction: row
`;





//Helper Functions that may need to be used in some elements like buttons
function isColorLight(color) {
    if (
        color == colors.WHITE ||
        color == colors.LIGHT ||
        color == colors.LIGHTGRAY ||
        color == colors.YELLOW) 
    {
        return true;
    }
    return false;
}